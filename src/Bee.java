import bos.RelativeMove;
import java.awt.*;
import java.util.Optional;
import java.util.List;
import java.util.function.Supplier;

public class Bee extends Character implements beeControl{

    public Bee(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        display = Optional.of(Color.YELLOW);
        this.behaviour = behaviour;
    }

    public void attack(Player player){
        System.out.println("Player has died");
        System.exit(0);
    }

    public void merge(beeControl target){

    }
}
