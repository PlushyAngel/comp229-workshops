import bos.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.time.*;
import java.util.function.BiConsumer;


public class Stage {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Player player;
    protected Character bee1;
    protected Character bee2;

    private List<Character> allCharacters;

    private ArrayList<BiConsumer<java.lang.Character, GameBoard<Cell>>> observers;

    private Instant timeOfLastMove;

    public Stage() {
        grid = new Grid(10, 10);
        shepherd = new Shepherd(grid.getRandomCell(), () -> new NoMove(grid, shepherd));
        sheep = new Sheep(grid.getRandomCell(), () -> grid.movesBetween(sheep.location, shepherd.location, sheep).get(0));
        wolf = new Wolf(grid.getRandomCell(), () -> grid.movesBetween(wolf.location, sheep.location, wolf).get(0));
        bee1 = new Bee(grid.getRandomCell(), () -> new MoveRandomly(grid, bee1));
        bee2 = new Bee(grid.getRandomCell(), () -> new MoveRandomly(grid, bee2));

        observers = new ArrayList();

        player = new Player(grid.getRandomCell());
        observers.add(player::notify);

        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
        allCharacters.add(bee1);
        allCharacters.add(bee2);
        timeOfLastMove = Instant.now();
    }

    public void update(){
        if(!player.inMove()) {
            if (timeOfLastMove.plus(Duration.ofSeconds(1)).isBefore(Instant.now())) {
                if (sheep.location == shepherd.location) {
                    System.out.println("Sheep is safe!");
                    System.exit(0);
                } else if (sheep.location == wolf.location) {
                    System.out.println("Sheep is dead :(");
                    System.exit(0);
                } else {
                    if (sheep.location.x == sheep.location.y) {
                        sheep.setBehaviour(() -> new MoveRandomly(grid, sheep));
                        shepherd.setBehaviour(() -> grid.movesBetween(shepherd.location, sheep.location, shepherd).get(0));
                    }
                    allCharacters.forEach((c) -> c.aiMove(this).perform());
                    player.startMove();
                    timeOfLastMove = Instant.now();
                }
            }
            Iterator<Character> it1 = allCharacters.iterator();
            while(it1.hasNext()){
                if(it1.next() instanceof Bee){
                    Iterator<Character> it2 = it1;
                    while(it2.hasNext()){
                        it2.next();

                    }
                }
            }
        }
    }

    public void paint(Graphics g, Point mouseLocation){
        grid.paint(g, mouseLocation);
        Iterator<Character> it = allCharacters.iterator();
        while(it.hasNext()){
            it.next().paint(g);
        }
        player.paint(g);
    }

    public void notifyAll(char c){
        for(BiConsumer bc : observers){
            bc.accept(new  java.lang.Character(c), grid);
        }
    }
}
