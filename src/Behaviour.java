import bos.RelativeMove;

@FunctionalInterface
public interface Behaviour {
    public RelativeMove chooseMove(Stage stage, Character mover);
}
