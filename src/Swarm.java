import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Supplier;

public class Swarm extends Character{

    private List<Bee> beesInSwarm;

    public Swarm(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        display = Optional.of(Color.PINK);
        this.behaviour = behaviour;
    }

    public void addBee(Bee bee){
        beesInSwarm.add(bee);
    }
}
