import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;

public class Week2 extends JFrame implements Runnable {

    private class Canvas extends JPanel implements KeyListener{

        private Stage stage;

        public Canvas() {
            setPreferredSize(new Dimension(1280, 720));
            stage = new Stage();
        }

        @Override
        public void paint(Graphics g) {
           stage.update();
           stage.paint(g, getMousePosition());
        }

        @Override
        public void keyTyped(KeyEvent e) {
            stage.notifyAll(e.getKeyChar());
        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    public static void main(String[] args) {
        Week2 window = new Week2();
        window.run();
    }

    private Week2() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        this.setContentPane(canvas);
        this.addKeyListener(canvas);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(20);
                this.repaint();
            }
        } catch(InterruptedException e){
            System.out.println(e.toString());
        }
    }
}

