import bos.RelativeMove;

import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class Wolf extends Character{

    public Wolf(Cell location, Supplier<RelativeMove> behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.RED);
        this.behaviour = behaviour;
    }
}
