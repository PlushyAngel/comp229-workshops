import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.function.Supplier;

public class Shepherd extends Character{

    public Shepherd(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        display = Optional.of(Color.GREEN);
        this.behaviour = behaviour;
    }
}
