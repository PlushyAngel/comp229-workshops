import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.List;
import java.util.function.Supplier;

public class Sheep extends Character{

    public Sheep(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        display = Optional.of(Color.WHITE);
        this.behaviour = behaviour;
    }
}
