import bos.GamePiece;
import bos.Pair;
import bos.GameBoard;
import bos.RelativeMove;
import bos.MoveRight;
import bos.MoveLeft;
import bos.MoveUp;
import bos.MoveDown;

import java.awt.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.List;

public class Grid implements GameBoard<Cell> {
    private Cell[][] grid = new Cell[20][20];

    private int x;
    private int y;

    public Grid(int x, int y){
        this.x = x;
        this.y = y;

        for (int i = 0; i < grid.length; i++ ){
            for (int j = 0; j < grid[x].length; j++){
                grid[i][j] = new Cell(x + j*35, y + i*35);
            }
        }
    }

    public void paint(Graphics g, Point mousePosition){
        doToEachCell((c) -> c.paint(g, c.contains(mousePosition)));
    }

    public Cell getRandomCell(){
        java.util.Random rand = new java.util.Random();
        return grid[rand.nextInt(20)][rand.nextInt(20)];
    }

    private bos.Pair<Integer, Integer> indexOfCell(Cell c){
        for (int y = 0; y < 20; y++){
            for(int x = 0; x < 20; x++){
                if(grid[y][x] == c) {
                    return new bos.Pair(y, x);
                }
            }
        }
        return null;
    }

    /**
     * Takes a cell consumer (i.e. a function that has a single `Cell` argument and
     * returns `void`) and applies that consumer to each cell in the grid.
     * @param func The `Cell` to `void` function to apply at each spot.
     */
    public void doToEachCell(Consumer<Cell> func){
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 20; x++) {
                func.accept(grid[y][x]);
            }
        }
    }

    /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and
     * returns a `boolean` result) and applies that predicate to each cell, returning
     * the first cell it finds for which the predicate is true.
     * @param predicate The `Cell` to `boolean` function to test with
     * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns `null` if no such cell found.
     */
    public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate){
        // Your job to fill in the body
        for(int y = 0; y < 20; y++){
            for(int x = 0; x < 20; x++){
                if(predicate.test(grid[y][x])){
                    return new Pair(y,x);
                }
            }
        }
        return null;
    }

    /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and
     * returns a `result` and applies that predicate to each cell, returning
     * the first cell it finds for which the predicate is true.  Returns an optional that is full
     * if such a cell is found and an empty optional if there is no such cell.
     * @param predicate The `Cell` to `boolean` function to test with
     * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns an empty optional if no cell found.
     */
    public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate){
        // Your job to fill in the body
        for(int y = 0; y < 20; y++){
            for(int x = 0; x < 20; x++){
                if(predicate.test(grid[y][x])){
                    return Optional.of(new Pair(y,x));
                }
            }
        }
        return Optional.empty();
    }



    @Override
    public Optional<Cell> below(Cell cell){
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.first < 19)
                .map((pair) -> grid[pair.first + 1][pair.second]);
    }

    @Override
    public Optional<Cell> above(Cell cell){
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.first > 0)
                .map((pair) -> grid[pair.first - 1][pair.second]);
    }

    @Override
    public Optional<Cell> rightOf(Cell cell){
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.second < 19)
                .map((pair) -> grid[pair.first][pair.second + 1]);
    }

    @Override
    public Optional<Cell> leftOf(Cell cell){
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.second > 0)
                .map((pair) -> grid[pair.first][pair.second - 1]);
    }

    @Override
    public List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> mover){
        Pair<Integer, Integer> fromIndex = findAmongstCells((c) -> c == from);
        Pair<Integer, Integer> toIndex = findAmongstCells((c) -> c == to);

        List<RelativeMove> result = new ArrayList<RelativeMove>();

        //horizontal movement
        if (fromIndex.second <= toIndex.second){
            //move right
            for(int i = fromIndex.second; i < toIndex.second; i++){
                result.add(new MoveRight(this, mover));
            }
        } else{
            //move left
            for(int i = toIndex.second; i < fromIndex.second; i++){
                result.add(new MoveLeft(this, mover));
            }
        }

        //vertical movement
        if (fromIndex.first <= toIndex.first){
            //move down
            for(int i = fromIndex.first; i < toIndex.first; i++){
                result.add(new MoveDown(this, mover));
            }
        } else{
            //move up
            for(int i = toIndex.first; i < fromIndex.first; i++){
                result.add(new MoveUp(this, mover));
            }
        }

        return result;
    }
}
