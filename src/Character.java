import java.awt.*;
import java.util.Optional;
import bos.RelativeMove;
import java.util.function.Supplier;

//Don't need abstract, does same thing as optional
public abstract class Character implements bos.GamePiece<Cell>{
    Optional<Color> display;
    Cell location;
    Supplier<RelativeMove> behaviour;

    public Character(Cell location, Supplier<RelativeMove> behaviour){
        this.location = location;
        //so you won't get null pointer exception
        this.display = Optional.empty();
        this.behaviour = behaviour;
    }

    public void paint(Graphics g){
        //if the display color exists, do this
        if(display.isPresent()){
            g.setColor(display.get());
            g.fillOval(location.x + location.width/4, location.y + location.height/4, location.width/2, location.height/2);
        }

        //if there is no display color, print out the class name with no color
        else{
            Class<?> enclosingClass = getClass().getEnclosingClass();
            if (enclosingClass != null) {
                System.out.println(enclosingClass.getName());
                } else {
                System.out.println(getClass().getName());
            }
        }
    }

    public Cell getLocationOf(){
        return location;
    }

    public void setLocationOf(Cell c){
        location = c;
    }

    public RelativeMove aiMove(Stage stage){
        return behaviour.get();
    }

    public void setBehaviour(Supplier<RelativeMove> behaviour) {
        this.behaviour = behaviour;
    }
}
